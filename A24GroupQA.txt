Section 1

1) Explain the difference between an abstract class and an interface?

   Abstract class							Interface
   The abstract keyword is used to declare abstract class.		The interface keyword is used to declare interface.
   It can be extended using keyword extends				It can be implemented using keyword implements
   One have one inheritance						Can have multiple inheritance
   Can provide complete, default code or details that can be 		Cannot provide any code just the signature
   overriden

2) What is the purpose of getters and setters in a class?
   To set security to the properties of the class, we declare the properties as private. 
   Properties combine aspects of both fields and methods which typically share name with fields.
   Getters is used to return the property value and is often made public
   Setters is used to set the property value and is often made public

Section 2

3) Explain the purpose of black box testing?
   Black box testing is a method of software testing that examines the functionality of an application without looking into its internal structures of working.
   This method is applied at every level of software testing, unit, integration, system and acceptance.
   Tests are based on requirements and functionality.

4) In your opinion what are the benefits of test cases?
   Test cases allow you to have consistency in your system, when you release every aspect of the system is tested from version 1 to the latest.
   Test cases permit you to cover all aspect of your system positive and negative.
   Writing test cases helps in the sense that it does not depend on the memory of the software engineer to test features of the system.

Section 3

5) What is an error first callback, and what is the reason for this pattern existing?
   Error first callback is a way to call multiple asynchronous processes one after another, you check in your first call if there's an error and pass it to the second call.
   The reason for this is that you simply terminate the execution of the application when there's an error, try and catch mechanism will handle the error.

6) Explain the difference between fs.readSync and fs.read (File System module in Nodejs)
   fs.readySync								fs.read
   Read the file synchronously by giving it the path of the		Read the file asynchronously by giving the path, encoding and callback function to check if there errors or not
   file and the encoding.						and terminate the application.

7) What tasks do you feel should be done asynchronously?
   Connecting to the database should be asynchronous.
   Saving data to the database.
   Application Program Interface verbs get, post, put and delete.



const fruitbasketclass = require('./fruitbasket.class');
const data             = require('./input');
const fruitbasket      = new fruitbasketclass(data);

test('Retrieves fruit basket id numbers', () => {
    expect(fruitbasket.fruitTotal()).toBe(3)
});
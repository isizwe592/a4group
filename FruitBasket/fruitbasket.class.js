class FruitBasketClass
{
    constructor(param) {
        this.fruits = param;
    }

    fruitTotal = () => this.fruits[0]['contents'].length;

    getId = () => this.fruits[0]['id'];

    fruitType() {
        let result = [];
        let counts = {};
        this.fruits[0]['contents'].forEach((list) => {
            // destruct
            const {type} = list;
            counts[type] = (counts[type] || 0) + 1;
        });

        return counts;
    }

    fruitWeight() {
        let sum = 0;
        this.fruits[0]['contents'].forEach((list) => {
            const {weight} = list;
            sum += weight;
        });

        return sum;
    }
}

module.exports = FruitBasketClass;
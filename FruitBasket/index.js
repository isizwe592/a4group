const data             = require('./input');
const fruitbasketclass = require('./fruitbasket.class');
const fs               = require('fs');
const fruit            = new fruitbasketclass(data);

let result             = [];

result.push({id: fruit.getId()});
result.push({total_fruits: fruit.fruitTotal()});
result.push({fruit_counts: fruit.fruitType()});

fs.promises.writeFile("output.json", JSON.stringify(result))
  .then(() => console.log("Write success"))
  .catch(err => console.log(err));

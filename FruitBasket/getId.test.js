const fruitbasketclass = require('./fruitbasket.class');
const data             = require('./input');
const fruitbasket      = new fruitbasketclass(data);

test('Retrieves fruit basket id numbers', () => {
    expect(fruitbasket.getId()).toBe("1ceb8c95-736f-4da3-86d9-86d55893b38a")
});
const fs = require('fs');
const path = process.argv[2];

fs.promises.access(path).then(() => {
    console.log('exists');
    fs.promises.stat(path).then((stats) => {
        console.log(stats);
        if (stats.size > 0) {
            fs.promises.readFile(path).then((buffer) => {
                console.log('File was found and the contents were loaded');
            }).catch(err => console.log("Error trying to read the stats"));
        } else {
            console.log('File exists but there is no content');
        }
    }).catch(err => console.log("Error trying to get stats"));
}).catch(err => console.log("File does not exist"));
